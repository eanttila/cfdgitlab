<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity
 */
class User implements UserInterface, \JsonSerializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $password;
	
	/**
     * @ORM\Column(type="string", length=500)
     */
    private $subscription;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    public function __construct($username)
    {
        $this->isActive = true;
        $this->username = $username;
    }
	
	public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

	public function getSubscription()
    {
        return $this->subscription;
    }
	
    public function getSalt()
    {
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }
	
	public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
    }
	
	public function jsonSerialize()
	{
		return [
			"id" => $this->getId(),
			"username" => $this->getUsername(),
			"subscription" => $this->getSubscription()
		];
	}
}