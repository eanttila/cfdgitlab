<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }
	
	/*
	public function findFactorsUsingRange($searchFrom, $useRange) {
		$rangeStart = 1292670089; //2010
		$rangeEnd = 1450436489; //2015

		$data = $this->createQueryBuilder('p');
		$data = $data->where('p.date > :rangeStart')
			->andWhere('p.date < :rangeEnd')
			->setParameter('rangeStart', $rangeStart)
			->setParameter('rangeEnd', $rangeEnd);
		
		$data = $data
			->orderBy('p.id', 'ASC')
			->setMaxResults(100)
			->getQuery()
			->getResult();
		
		return $data;
    }*/

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}