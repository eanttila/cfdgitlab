<?php

 namespace App\Controller;
 
 use Symfony\Component\HttpFoundation\Response;
 use App\Entity\Factor;
 use App\Repository\FactorRepository;
 use App\Repository\UserRepository;
 use Doctrine\ORM\EntityManagerInterface;
 use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
 use Symfony\Component\HttpFoundation\JsonResponse;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\Routing\Annotation\Route;

 /**
  * Class FactorController
  * @package App\Controller
  * @Route("/manage", name="manage_api")
  */
 class ManageController extends AbstractController
 {
	/**
	* @param UserRepository $userRepository
	* @return JsonResponse
	* @Route("/users", name="users", methods={"GET"})
	*/
	public function getUsers(UserRepository $userRepository){
		 $data = $userRepository->findAll();
		 return $this->response($data);
	}
	
	/**
   * @param UserRepository $userRepository
   * @param $id
   * @return JsonResponse
   * @Route("/users/{id}", name="users_get", methods={"GET"})
   */
	public function getManagerUser(UserRepository $userRepository, $id){
	$user = $userRepository->find($id);

	if (!$user){
	$data = [
	 'status' => 404,
	 'errors' => "User not found",
	];
	return $this->response($data, 404);
	}
	return $this->response($user);
	}
	
	/**
	* Returns a JSON response
	*
	* @param array $data
	* @param $status
	* @param array $headers
	* @return JsonResponse
	*/
	public function response($data, $status = 200, $headers = []) {
		return new JsonResponse($data, $status, $headers);
	}

	protected function transformJsonBody(\Symfony\Component\HttpFoundation\Request $request) {
		$data = json_decode($request->getContent(), true);

		if ($data === null) {
		return $request;
		}

		$request->request->replace($data);

		return $request;
	}	
	

 }