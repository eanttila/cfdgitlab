<?php
//src/EventListener
/**
 * Created by PhpStorm.
 * User: younesdiouri
 * Date: 01/03/2019
 * Time: 18:04
 */
namespace App\App\EventListener;
use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\User\UserInterface;

class JWTCreatedListener
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();
        $payload = $event->getData();
        /** @var User $user */
        $user = $event->getUser();

        if (!$user instanceof UserInterface) {
            throw new \Exception('Does not match user interface', 500);
        }

        //$payload['id'] = 123;
        $payload['subscription'] = $user->getSubscription();
        $event->setData($payload);
    }
}

