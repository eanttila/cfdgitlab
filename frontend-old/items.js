
// The ItemComponent matches one of two different routes
// depending on the full pathname
const ReactBootstrapComponent = ReactBootstrap;
const ItemComponent = ({ items, setItems }) => {
	React.useEffect(() => {
		
	}, [])
	
return(
  <Switch>
    <Route exact path={['/items']} render={() => ( <Items items={items} setItems={setItems} /> )}/>
	<Route path={["/items/:id"]} render={({ match }) => {
		const itemId = match.params.id
		const filteredItem = items.find(x => x.id == itemId)
		//console.log(filteredItem)
        return <Item itemId={itemId} filteredItem={filteredItem} />;
    }} />
  </Switch>
)}



// The Items iterates over all of the items and creates
// a link to their profile page.
const Items = ({ items, setItems }) => {
	React.useEffect(() => {
		setNewItems(items)
		getItems()
	}, [])
	
	const [newItems, setNewItems] = React.useState([]);
	const [searchDetails, setSearchDetails] = React.useState('Haetaan viimeisimmät tuotteet... ');
	
	const debounce = (fn, delay = 400) => {
		let timeoutId;
		return (...args) => {
			if (timeoutId) {
				clearTimeout(timeoutId);
			}
			timeoutId = setTimeout(() => {
				fn.apply(null, args)
			}, delay);
		};
	};

	const searchItems = debounce(async (searchParam) => {
		if (searchParam.length > 1) {
			fetch('http://localhost/cfd/factorapi/search/'+searchParam).then(function (response) {
				return response.json();
			}).then(function (json) {
				setNewItems(json)
				setSearchDetails(`Haetaan tuotteet hakusanalla "${searchParam}", ${json.length} tulosta`)
			}).catch(function (err) {
				console.warn('Something went wrong.', err);
				alert(err)
			})
		} else if (searchParam.length < 1) {
			getItems()
		}
	});
	
	const getItems = () => {
		fetch('http://localhost/cfd/factorapi/factors').then(function (response) {
			return response.json();
		}).then(function (json) {
			setNewItems(json)
			setSearchDetails(`Getting the latest products, ${json.length} result(s)`)
		}).catch(function (err) {
			console.warn('Something went wrong.', err);
			alert(err)
		})
	};

  return (
  <div>
	<ReactBootstrapComponent.Breadcrumb>
	<ReactBootstrapComponent.Breadcrumb.Item href="/cfd/#/">Home</ReactBootstrapComponent.Breadcrumb.Item>
	<ReactBootstrapComponent.Breadcrumb.Item active>Factors</ReactBootstrapComponent.Breadcrumb.Item>
	</ReactBootstrapComponent.Breadcrumb>

  <h3><u>Factors and products</u></h3>
	<label style={{marginBottom: '0px'}}><b>Search products:</b></label><br/>
	<i>{searchDetails}</i><br/>
	<input className="form-control input-sm" style={{marginTop: '3px', maxWidth: '800px', marginBottom: '30px'}} type="text" onChange={e =>searchItems(e.target.value)} />
	<label><b>Found products:</b></label>
    {/*<ul style={{paddingLeft: '14px'}}>
      {
        newItems.map(p => (
          <li key={p.id}>
            <Link to={`/items/${p.id}`}>{p.brand}</Link>
          </li>
        ))
      }
    </ul>*/}
    <div className="row" style={{marginLeft: '0px',border: '1px solid lightGray', maxWidth: '1200px', boxShadow: '1px 1px 10px #808080'}}>
	  <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Category</b></div>
	  <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Group</b></div>
      <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Brand</b></div>
      <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Model</b></div>
	  <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Version</b></div>
	  <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>
	  <ReactBootstrapComponent.OverlayTrigger
      key='Total'
      placement='top'
      overlay={
        <ReactBootstrapComponent.Tooltip id={`tooltip-top`}>
          <strong>Total</strong> emissions / total emission deviation
        </ReactBootstrapComponent.Tooltip>
      }>
      <b>Total/dev</b>
		</ReactBootstrapComponent.OverlayTrigger>
	  </div>	  
	  <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>
	  <ReactBootstrapComponent.OverlayTrigger
      key='Operation'
      placement='top'
      overlay={
        <ReactBootstrapComponent.Tooltip id={`tooltip-top`}>
          <strong>Operation</strong> emissions / operation emission deviation
        </ReactBootstrapComponent.Tooltip>
      }>
      <b>Oper/dev</b>
		</ReactBootstrapComponent.OverlayTrigger>
	  </div>	  
	  <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>
	  <ReactBootstrapComponent.OverlayTrigger
      key='Production'
      placement='top'
      overlay={
        <ReactBootstrapComponent.Tooltip id={`tooltip-top`}>
          <strong>Production</strong> emissions / production emission deviation
        </ReactBootstrapComponent.Tooltip>
      }>
      <b>Prod/dev</b>
		</ReactBootstrapComponent.OverlayTrigger>
	  </div>	  
	  <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>
	  <ReactBootstrapComponent.OverlayTrigger
      key='Production'
      placement='top'
      overlay={
        <ReactBootstrapComponent.Tooltip id={`tooltip-top`}>
          <strong>Disposal</strong> emissions / disposal emission deviation
        </ReactBootstrapComponent.Tooltip>
      }>
      <b>Disp/dev</b>
		</ReactBootstrapComponent.OverlayTrigger>
	  </div>
	  <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Added</b></div>
      <div className="col" style={{padding: '10px', backgroundColor: 'white', fontSize: '14px'}}><b>Details</b></div>
    </div>
	{
	newItems.map(p => {
		return(
		<div key={p.id} className="row" style={{marginLeft: '0px', border: '1px solid lightGray', maxWidth: '1200px', boxShadow: '1px 1px 10px #808080'}}>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>{p.category}</div>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>{p.group}</div>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', maxWidth: '120px', fontSize: '14px'}}>{p.brand}</div>
		  <div className="col" style={{overflow: 'auto', padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', maxWidth: '240px', fontSize: '14px'}}>{p.model}</div>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>{p.versionInfo}</div>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '13px'}}>
		  { p.category == 'vehicle' ? <div>{p.totalCO2}<b>/</b>({p.totalCO2dev}) {p.emissionUnit}</div> : <div>{p.totalCO2/1000}<b>/</b>({p.totalCO2dev/1000}) {p.emissionUnit}</div> }
		  </div>
		  <div className="col" style={{padding: '5px',borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '13px'}}>
		  { p.category == 'vehicle' ? <div>{p.operationCO2}<b>/</b>({p.operationCO2dev}) {p.emissionUnit}</div> : <div>{p.operationCO2/1000}<b>/</b>({p.operationCO2dev/1000}) {p.emissionUnit}</div> }
		  </div>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '13px'}}>
		  { p.category == 'vehicle' ? <div>{p.productionCO2}<b>/</b>({p.productionCO2dev}) {p.emissionUnit}</div> : <div>{p.productionCO2/1000}<b>/</b>({p.productionCO2dev/1000}) {p.emissionUnit}</div> }
		  </div>
		  <div className="col" style={{padding: '5px',borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '13px'}}>
		  { p.category == 'vehicle' ? <div>{p.disposalCO2}<b>/</b>({p.disposalCO2dev}) {p.emissionUnit}</div> : <div>{p.disposalCO2/1000}<b>/</b>({p.disposalCO2dev/1000}) {p.emissionUnit}</div> }
		  </div>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>{p.date}</div>
		  <div className="col" style={{padding: '5px', backgroundColor: 'white', fontSize: '14px'}}><Link to={`/items/${p.id}`}>
		  <button type="submit" className="btn btn-info" style={{paddingTop: '2px', paddingBottom: '2px', paddingLeft: '15px', paddingRight: '15px', fontSize: '14px'}}>Show</button>
		  </Link></div>
    </div>)})
	}	
  </div>
)}

// The Item looks up the item using the id parsed from
// the URL's pathname. If no item is found with the given
// id, then a "item not found" message is displayed.
const Item = ({itemId, filteredItem}) => {
	React.useEffect(() => {
		document.documentElement.scrollTop = 0
		if (filteredItem) {
			setItem(filteredItem)
		}
		fetch('http://localhost/cfd/factorapi/factors/'+itemId).then(function (response) {
			return response.json();
		}).then(function (json) {
			setItem(json)
		}).catch(function (err) {
			console.warn('Something went wrong.', err);
			alert(err)
		})	
	}, [])
	
	const [item, setItem] = React.useState({
		extraData: ''
	});
  
  if (!item) {
    return <div>Sorry, but the factor was not found</div>
  }
  return (
  <div>
  	<ReactBootstrapComponent.Breadcrumb>
	<ReactBootstrapComponent.Breadcrumb.Item href="/cfd/#/">Home</ReactBootstrapComponent.Breadcrumb.Item>
	<ReactBootstrapComponent.Breadcrumb.Item href="/cfd/#/items/">
	Factors
	</ReactBootstrapComponent.Breadcrumb.Item>
	<ReactBootstrapComponent.Breadcrumb.Item active>Factor</ReactBootstrapComponent.Breadcrumb.Item>
	</ReactBootstrapComponent.Breadcrumb>
    <div>
	<Link to='/items'><button type="submit" className="btn btn-info" style={{paddingTop: '2px', paddingBottom: '2px', paddingLeft: '15px', paddingRight: '15px', fontSize: '14px'}}>Back</button></Link>	
	{ item.extraData.includes('icecatProduct=') ? <Link to={'/products/'+itemId}><button type="submit" className="btn btn-info" style={{marginLeft: '5px', paddingTop: '2px', paddingBottom: '2px', paddingLeft: '15px', paddingRight: '15px', fontSize: '14px'}}>Icecat details</button></Link> : '' }
    </div>
	
	<div className="row" style={{marginTop: '30px'}}>
		<div className="col" style={{borderLeft: '1px solid lightGray', borderRight: '1px solid lightGray', borderTop: '1px solid lightGray', backgroundColor: '#f2f2f2', boxShadow: '1px 1px 4px #808080', marginBottom: '5px'}}>
			<b>ID:</b> {item.id}
		</div>
	</div>
	<div className="row" style={{marginTop: '0px'}}>
    <div className="col" style={{borderBottom: '1px solid lightGray', borderLeft: '1px solid lightGray', borderRight: '1px solid lightGray', borderTop: '1px solid lightGray', height: '800px'}}>
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Category</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Group</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>{item.group}&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>{item.category}</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Brand</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Model</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>{item.brand}&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>{item.model}</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Data source URL</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
		<div className="col" style={{display: 'grid', padding: '7px', paddingTop: '3px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}><div><a href={item.dataSourceUrl} style={{fontSize: '14px', paddingTop: '2px', paddingBottom: '2px', marginBottom: '2px'}} className="btn btn-info" role="button">Link to Data source</a></div></div>
		</div>
		{/*# EMISSION START #*/}
		<div className="row" style={{borderTop: '1px solid lightGray', marginTop: '30px'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Total CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Total CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>
			{ item.category == 'vehicle' ? <div>{item.totalCO2} {item.emissionUnit} &nbsp;</div> : <div>{item.totalCO2/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>
			{ item.category == 'vehicle' ? <div>{item.totalCO2dev} {item.emissionUnit} &nbsp;</div> : <div>{item.totalCO2dev/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Production CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Production CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>
			{ item.category == 'vehicle' ? <div>{item.productionCO2} {item.emissionUnit} &nbsp;</div> : <div>{item.productionCO2/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>
			{ item.category == 'vehicle' ? <div>{item.productionCO2dev} {item.emissionUnit} &nbsp;</div> : <div>{item.productionCO2dev/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Operation CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Operation CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>
			{ item.category == 'vehicle' ? <div>{item.operationCO2} {item.emissionUnit} &nbsp;</div> : <div>{item.operationCO2/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>
			{ item.category == 'vehicle' ? <div>{item.operationCO2dev} {item.emissionUnit} &nbsp;</div> : <div>{item.operationCO2dev/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Disposal CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Disposal CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>
			{ item.category == 'vehicle' ? <div>{item.disposalCO2} {item.emissionUnit} &nbsp;</div> : <div>{item.disposalCO2/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>
			{ item.category == 'vehicle' ? <div>{item.disposalCO2dev} {item.emissionUnit} &nbsp;</div> : <div>{item.disposalCO2dev/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
		</div>
		{/*# EMISSION END #*/}
		<div className="row" style={{borderTop: '1px solid lightGray', marginTop: '40px'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>EAN</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>UPC</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>{item.ean}&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>{item.upc}</div>
		</div>
		<div className="row" style={{borderTop: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Extra data</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Source</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>{item.extraData}&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>{item.source}</div>
		</div>
		<div className="row" style={{borderTop: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Version Info</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>External ID</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>{item.versionInfo}&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>{item.externalId}</div>
		</div>
		
    </div>
    <div className="col" style={{padding: '0px', borderTop: '1px solid lightGray', borderRight: '1px solid lightGray', borderBottom: '1px solid lightGray', height: '800px'}}>
	<div style={{border: '1px solid black', margin: 'auto', height: '250px', width: '250px', backgroundImage: 'url(https://greencarbon.fi/wp-content/uploads/2020/12/placeholder.png)', backgroundPosition: 'center', backgroundRepeat: 'no-repeat'}}>
	</div>
    </div>
  </div>
  {/*
  <div className="row">
    <div className="col" style={{borderTop: '1px solid lightGray', borderBottom: '1px solid lightGray', borderRight: '1px solid lightGray', borderLeft: '1px solid lightGray', height: '200px'}}>
      1 of 3
    </div>
    <div className="col" style={{borderTop: '1px solid lightGray', borderBottom: '1px solid lightGray', borderRight: '1px solid lightGray', height: '200px'}}>
      2 of 3
    </div>
    <div className="col" style={{borderBottom: '1px solid lightGray', borderTop: '1px solid lightGray', borderRight: '1px solid lightGray', height: '200px'}}>
      3 of 3
    </div>
  </div>
	*/}
  </div>
  )
}