import { Link, Route, Switch } from 'react-router-dom';
import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';

const PostsComponent = ({ areaName, items, setItems }) => {
	React.useEffect(() => {
		
	}, [])
	
// The Items iterates over all of the items and creates
// a link to their profile page.
const Posts = ({ items, setItems }) => {
	React.useEffect(() => {
		if (items.length > 1) {
			setFactors(items);
			setSearchDetails(`Got all factors, limited to ${items.length} result(s), not using range`)
		}
		//getFactors();
		const currentTime = new Date().toISOString().slice(0, 10);
		setRangeStart(currentTime);
		setRangeEnd(currentTime);
	}, [items])
	
	const [factors, setFactors] = React.useState([]);
	const [currentPage, setCurrentPage] = React.useState(1);
	const [itemsPerPage, setItemsPerPage] = React.useState(5);
	const [searchDetails, setSearchDetails] = React.useState('Getting the latest factors... ');
	const [searchword, setSearchword] = React.useState('');
	const [searchFrom, setSearchFrom] = React.useState('brand');
	const [useRange, setUseRage] = React.useState(false);
	const [rangeStart, setRangeStart] = React.useState('');
	const [rangeEnd, setRangeEnd] = React.useState('');
	const onEnter = (event, callback) => event.key === 'Enter' && callback()
	
	const doSearch = async () => {
		if (searchword === '') {
			getFactors()
			return
		}
		try {
			const rangeStartUnix = new Date(rangeStart).getTime()/1000;
			const rangeEndUnix = new Date(rangeEnd).getTime()/1000;
			const res = await fetch('http://localhost/cfd/factorapi/searchfactor/'+searchword+'?searchFrom='+searchFrom+'&useRange='+useRange+'&rangeStart='+rangeStartUnix+'&rangeEnd='+rangeEndUnix);
			if (!res.ok) {
				throw new Error("Fetch was not OK");
			}
			const json = await res.json();
			setCurrentPage(1)
			setFactors(json)
			if (useRange) {
				setSearchDetails(`Got factors with keyword "${searchword}" using "${searchFrom}" and time range from "${rangeStart}" to "${rangeEnd}", ${json.length} result(s)`)
			} else {
				setSearchDetails(`Got factors with keyword "${searchword}" using "${searchFrom}" with no time range, ${json.length} result(s)`)
			}	
		} catch (error) {
			alert(`Error fetching or parsing data: ${error}`);
	  }		
	};
	
	/*const debounce = (fn, delay = 400) => { 
		let timeoutId;
		return (...args) => {
			if (timeoutId) {
				clearTimeout(timeoutId);
			}
			timeoutId = setTimeout(() => {
				fn.apply(null, args)
			}, delay);
		};
	};
	
	const searchFactors = debounce(async (searchParam) => {
		if (searchParam.length > 1) {
			fetch('http://localhost/cfd/factorapi/search/'+searchParam).then(function (response) {
				return response.json();
			}).then(function (json) {
				setFactors(json)
				setSearchDetails(`Haetaan tuotteet hakusanalla "${searchParam}", ${json.length} tulosta`)
			}).catch(function (err) {
				console.warn('Something went wrong.', err);
				alert(err)
			})
		} else if (searchParam.length < 1) {
			getItems()
		}
	});*/
	
	const changeSearchFrom = (word) => {
		setSearchFrom(word)
	};
	
	const getFactors = () => {
		const rangeStartUnix = new Date(rangeStart).getTime()/1000;
		const rangeEndUnix = new Date(rangeEnd).getTime()/1000;
		fetch('http://localhost/cfd/factorapi/factors?useRange='+useRange+'&rangeStart='+rangeStartUnix+'&rangeEnd='+rangeEndUnix).then(function (response) {
			return response.json();
		}).then(function (json) {
			setFactors(json)
			setSearchDetails(`Got all factors, limited to ${json.length} result(s), not using range`)
		}).catch(function (err) {
			console.warn('Something went wrong.', err);
			alert(err)
		})
	};

	const handleClick = (event) => {
		setCurrentPage(Number(event.target.id));
	};
	
	const changeUseRange = (event) => {
		{ event === 'false' ? setUseRage(true) : setUseRage(false) }
	};
	
	const changeRangeStartOrEnd = (event) => {
		if (event.name === 'rangeStart') {
			setRangeStart(event.value);
		} else {
			setRangeEnd(event.value);
		}
	};
	
	const changeItemsPerPage = (number) => {
		setCurrentPage(1);
		setItemsPerPage(number);
	};
	
	const indexOfLastTodo = currentPage * itemsPerPage;
	const indexOfFirstTodo = indexOfLastTodo - itemsPerPage;
	const currentItems = factors.slice(indexOfFirstTodo, indexOfLastTodo);

	const pageNumbers = [];
	for (let i = 1; i <= Math.ceil(factors.length / itemsPerPage); i++) {
	  pageNumbers.push(i);
	}

	const renderPageNumbers = pageNumbers.map(number => {
		if (number === currentPage) {
			return (
			<li className="page-item" key={number}><a className="page-link" style={{cursor: 'not-allowed', background: 'lightGray'}} key={number} id={number} onClick={handleClick}>{number}</a></li>
			);
		} else {
			return (
			<li className="page-item" key={number}><a className="page-link" style={{cursor: 'pointer'}} key={number} id={number} onClick={handleClick}>{number}</a></li>
			);
		}
	});
	
	  return (
	  <div>
	  <label style={{marginBottom: '0px'}}><b>Search products:</b></label><br/>
	  <i>{searchDetails}</i><br/>
		{/*<input className="form-control input-sm" style={{marginTop: '3px', maxWidth: '800px', marginBottom: '30px'}} type="text" onChange={e =>searchFactors(e.target.value)} />*/}
		<input
		className="form-control input-sm" style={{marginTop: '3px', maxWidth: '900px', marginBottom: '0px'}}
		type="text" 
		placeholder="Write brand or model name and press Enter..." 
		onChange={e => setSearchword(e.target.value)}
		onKeyPress={e => onEnter(e, doSearch)}/>
		<div style={{paddingLeft: '8px', paddingBottom: '3px', border: '1px solid lightGray', borderRadius: '5px', marginTop: '2px', maxWidth: '900px'}}>
		<div style={{display: 'inline', paddingRight: '5px'}}>Search using:</div>
			<label style={{paddingRight: '5px', marginBottom: '0px'}}>
				<input
				style={{margin: '2px'}}
				type="radio"
				value="brand"
				checked={searchFrom === "brand"}
				onChange={e => changeSearchFrom(e.target.value)}
				/>
            Brand
          </label>
		  <label style={{paddingRight: '10px', marginBottom: '0px'}}>
				<input
				style={{margin: '2px'}}
				type="radio"
				value="model"
				checked={searchFrom === "model"}
				onChange={e => changeSearchFrom(e.target.value)}
				/>
            Model
          </label>		
		<label style={{paddingRight: '10px', marginBottom: '0px'}}>
		| Use range:
		<input
            name="useRange"
			value={useRange}
			type="checkbox"
			style={{marginLeft: '5px'}}
            checked={useRange}
            onChange={e => changeUseRange(e.target.value)} />
		</label>
		<label style={{paddingRight: '5px', marginBottom: '0px'}}>
		| Range start:
		<input
            name="rangeStart"
			type="date"
			style={{fontSize: '11px', marginLeft: '5px'}}
			defaultValue={rangeStart}
			disabled={!useRange}
            onChange={e => changeRangeStartOrEnd(e.target)} />
		</label>
		<label style={{paddingRight: '10px', marginBottom: '0px'}}>
		Range end:
		<input
            name="rangeEnd"
			type="date"
			style={{fontSize: '11px', marginLeft: '5px'}}
			defaultValue={rangeEnd}
			disabled={!useRange}
            onChange={e => changeRangeStartOrEnd(e.target)} />
		</label>
		</div>
		<hr style={{marginBottom: '0px'}}/>
		  <label style={{marginTop: '5px', marginRight: '5px'}}>Items per page:</label>
		  <select style={{paddingLeft: '5px', paddingRight: '5px', borderRadius: '5px'}} name="itemsPerPage" onChange={e => changeItemsPerPage(e.target.value)}>
			<option value="5">5</option>
			<option value="10">10</option>
			<option value="20">20</option>
			<option value="50">50</option>
		  </select>
		  <br/>
		<hr style={{marginTop: '0px'}}/>
		<label style={{}}><b>Found products:</b></label>
			<div className="row" style={{marginLeft: '0px',border: '1px solid lightGray', maxWidth: '1200px', boxShadow: '1px 1px 10px #808080'}}>
			<div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Category</b></div>
			<div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Group</b></div>
			<div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Brand</b></div>
			<div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Model</b></div>
			<div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Version</b></div>
			<div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Added</b></div>
			<div className="col" style={{padding: '10px', backgroundColor: 'white', fontSize: '14px'}}><b>Details</b></div>
			<div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>
			<ReactBootstrap.OverlayTrigger
			key='Total'
			placement='top'
			overlay={
			<ReactBootstrap.Tooltip id={`tooltip-top`}>
			  <strong>Total</strong> emissions / total emission deviation
			</ReactBootstrap.Tooltip>
			}>
			<b>Total/dev</b>
			</ReactBootstrap.OverlayTrigger>
			</div>	  
			<div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>
			<ReactBootstrap.OverlayTrigger
			key='Operation'
			placement='top'
			overlay={
			<ReactBootstrap.Tooltip id={`tooltip-top`}>
			  <strong>Operation</strong> emissions / operation emission deviation
			</ReactBootstrap.Tooltip>
			}>
			<b>Oper/dev</b>
			</ReactBootstrap.OverlayTrigger>
			</div>	  
			<div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>
			<ReactBootstrap.OverlayTrigger
			key='Production'
			placement='top'
			overlay={
			<ReactBootstrap.Tooltip id={`tooltip-top`}>
			  <strong>Production</strong> emissions / production emission deviation
			</ReactBootstrap.Tooltip>
			}>
			<b>Prod/dev</b>
			</ReactBootstrap.OverlayTrigger>
			</div>	  
			<div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>
			<ReactBootstrap.OverlayTrigger
			key='Production'
			placement='top'
			overlay={
			<ReactBootstrap.Tooltip id={`tooltip-top`}>
			  <strong>Disposal</strong> emissions / disposal emission deviation
			</ReactBootstrap.Tooltip>
			}>
			<b>Disp/dev</b>
			</ReactBootstrap.OverlayTrigger>
			</div>
			<div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Added</b></div>
			<div className="col" style={{padding: '10px', backgroundColor: 'white', fontSize: '14px'}}><b>Details</b></div>
			</div>
		{currentItems.map(p => {
			return(
			<div key={p.id} className="row" style={{marginLeft: '0px', border: '1px solid lightGray', maxWidth: '1200px', boxShadow: '1px 1px 10px #808080'}}>
			<div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>{p.category}</div>
			<div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>{p.group}</div>
			<div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', maxWidth: '120px', fontSize: '14px'}}>{p.brand}</div>
			<div className="col" style={{overflow: 'auto', padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', maxWidth: '240px', fontSize: '14px'}}>{p.model}</div>
			<div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>{p.versionInfo}</div>
			<div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '13px'}}>
			{ p.category === 'vehicle' ? <div>{p.totalCO2}<b>/</b>({p.totalCO2dev}) {p.emissionUnit}</div> : <div>{p.totalCO2/1000}<b>/</b>({p.totalCO2dev/1000}) {p.emissionUnit}</div> }
			</div>
			<div className="col" style={{padding: '5px',borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '13px'}}>
			{ p.category === 'vehicle' ? <div>{p.operationCO2}<b>/</b>({p.operationCO2dev}) {p.emissionUnit}</div> : <div>{p.operationCO2/1000}<b>/</b>({p.operationCO2dev/1000}) {p.emissionUnit}</div> }
			</div>
			<div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '13px'}}>
			{ p.category === 'vehicle' ? <div>{p.productionCO2}<b>/</b>({p.productionCO2dev}) {p.emissionUnit}</div> : <div>{p.productionCO2/1000}<b>/</b>({p.productionCO2dev/1000}) {p.emissionUnit}</div> }
			</div>
			<div className="col" style={{padding: '5px',borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '13px'}}>
			{ p.category === 'vehicle' ? <div>{p.disposalCO2}<b>/</b>({p.disposalCO2dev}) {p.emissionUnit}</div> : <div>{p.disposalCO2/1000}<b>/</b>({p.disposalCO2dev/1000}) {p.emissionUnit}</div> }
			</div>
			<div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>{p.date}</div>
			<div className="col" style={{padding: '5px', backgroundColor: 'white', fontSize: '14px'}}><Link to={`/posts/${p.id}`}>
			<button type="submit" className="btn btn-info" style={{paddingTop: '2px', paddingBottom: '2px', paddingLeft: '15px', paddingRight: '15px', fontSize: '14px'}}>Show</button>
			</Link></div>
			</div>)})
		}
		<hr />
		<ul id="page-numbers" className="pagination" style={{marginTop: '10px'}}>
		  {renderPageNumbers}
		</ul>
	  </div>
	)
}

// The Item looks up the item using the id parsed from
// the URL's pathname. If no item is found with the given
// id, then a "item not found" message is displayed.
const Post = ({itemId}) => {
	React.useEffect(() => {
		document.documentElement.scrollTop = 0;
		fetch('http://localhost/cfd/factorapi/factors/'+itemId).then(function (response) {
			return response.json();
		}).then(function (json) {
			setItem(json)
		}).catch(function (err) {
			console.warn('Something went wrong.', err);
			alert(err)
		})
	}, [itemId])
	
	const [item, setItem] = React.useState(false);
  
  if (!item) {
    return <div>Still loading...</div>
  }
  return (
  <div>
	
	<div className="row" style={{marginTop: '30px'}}>
		<div className="col" style={{borderLeft: '1px solid lightGray', borderRight: '1px solid lightGray', borderTop: '1px solid lightGray', backgroundColor: '#f2f2f2', boxShadow: '1px 1px 4px #808080', marginBottom: '5px'}}>
			<b>ID:</b> {item.id}
		</div>
	</div>
	<div className="row" style={{marginTop: '0px'}}>
    <div className="col" style={{borderBottom: '1px solid lightGray', borderLeft: '1px solid lightGray', borderRight: '1px solid lightGray', borderTop: '1px solid lightGray', height: '800px'}}>
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Category</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Group</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>{item.group}&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>{item.category}</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Brand</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Model</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>{item.brand}&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>{item.model}</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Data source URL</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
		<div className="col" style={{display: 'grid', padding: '7px', paddingTop: '3px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}><div><a href={item.dataSourceUrl} style={{fontSize: '14px', paddingTop: '2px', paddingBottom: '2px', marginBottom: '2px'}} className="btn btn-info" role="button">Link to Data source</a></div></div>
		</div>
		{/*# EMISSION START #*/}
		<div className="row" style={{borderTop: '1px solid lightGray', marginTop: '30px'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Total CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Total CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>
			{ item.category === 'vehicle' ? <div>{item.totalCO2} {item.emissionUnit} &nbsp;</div> : <div>{item.totalCO2/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>
			{ item.category === 'vehicle' ? <div>{item.totalCO2dev} {item.emissionUnit} &nbsp;</div> : <div>{item.totalCO2dev/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Production CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Production CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>
			{ item.category === 'vehicle' ? <div>{item.productionCO2} {item.emissionUnit} &nbsp;</div> : <div>{item.productionCO2/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>
			{ item.category === 'vehicle' ? <div>{item.productionCO2dev} {item.emissionUnit} &nbsp;</div> : <div>{item.productionCO2dev/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Operation CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Operation CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>
			{ item.category === 'vehicle' ? <div>{item.operationCO2} {item.emissionUnit} &nbsp;</div> : <div>{item.operationCO2/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>
			{ item.category === 'vehicle' ? <div>{item.operationCO2dev} {item.emissionUnit} &nbsp;</div> : <div>{item.operationCO2dev/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Disposal CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Disposal CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>
			{ item.category === 'vehicle' ? <div>{item.disposalCO2} {item.emissionUnit} &nbsp;</div> : <div>{item.disposalCO2/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>
			{ item.category === 'vehicle' ? <div>{item.disposalCO2dev} {item.emissionUnit} &nbsp;</div> : <div>{item.disposalCO2dev/1000} {item.emissionUnit} &nbsp;</div> }
			</div>
		</div>
		{/*# EMISSION END #*/}
		<div className="row" style={{borderTop: '1px solid lightGray', marginTop: '40px'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>EAN</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>UPC</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>{item.ean}&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>{item.upc}</div>
		</div>
		<div className="row" style={{borderTop: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Extra data</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Source</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>{item.extraData}&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>{item.source}</div>
		</div>
		<div className="row" style={{borderTop: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Version Info</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>External ID</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>{item.versionInfo}&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>{item.externalId}</div>
		</div>
		
    </div>
    <div className="col" style={{padding: '0px', borderTop: '1px solid lightGray', borderRight: '1px solid lightGray', borderBottom: '1px solid lightGray', height: '800px'}}>
	<div style={{border: '1px solid black', margin: 'auto', height: '250px', width: '250px', backgroundImage: 'url(https://greencarbon.fi/wp-content/uploads/2020/12/placeholder.png)', backgroundPosition: 'center', backgroundRepeat: 'no-repeat'}}>
	</div>
    </div>
  </div>
  {/*
  <div className="row">
    <div className="col" style={{borderTop: '1px solid lightGray', borderBottom: '1px solid lightGray', borderRight: '1px solid lightGray', borderLeft: '1px solid lightGray', height: '200px'}}>
      1 of 3
    </div>
    <div className="col" style={{borderTop: '1px solid lightGray', borderBottom: '1px solid lightGray', borderRight: '1px solid lightGray', height: '200px'}}>
      2 of 3
    </div>
    <div className="col" style={{borderBottom: '1px solid lightGray', borderTop: '1px solid lightGray', borderRight: '1px solid lightGray', height: '200px'}}>
      3 of 3
    </div>
  </div>
	*/}
  </div>
  )
}

return(
  <Switch>
    <Route exact path={['/posts']} render={() => ( <Posts items={items} setItems={setItems} /> )}/>
	<Route path={["/posts/:id"]} render={({ match }) => {
		const itemId = match.params.id
		const filteredItem = items.find(x => x.id === itemId)
		//console.log(filteredItem)
        return <Post itemId={itemId} />;
    }} />
  </Switch>
)}

export default PostsComponent;