import { useHistory , Link, Route, Switch, useRouteMatch } from 'react-router-dom';
import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';

const ProfileComponent = ({ isLoggedIn }) => {
	const { path } = useRouteMatch();
	React.useEffect(() => {
		const token = localStorage.getItem("token");
		if (token && isLoggedIn) {
			const token = localStorage.getItem("token");
			checkManagement(token);
		} else {
			history.push("/");
		}
	}, [isLoggedIn])
	
	const history = useHistory();
	const [user, setUser] = React.useState(false);
	const [allowManage, setAllowManage] = React.useState(false);
	
	const checkManagement = async (token) => {
		try {
			const res = await fetch('http://localhost/cfd/auth/profile', { headers: {"Authorization" : `Bearer ${token}`} });
			if (!res.ok) {
				throw new Error("Fetch was not OK");
			}
			const json = await res.json();
			if (json.subscription === 'admin') {
				setAllowManage(true);
			}
			setUser(json);
		} catch (error) {
			localStorage.clear();
			//alert(`Error fetching or parsing data: ${error}`);
	  }		
	};
	
const Manage = ({}) => {
	React.useEffect(() => {
		const token = localStorage.getItem("token");
		if (token) {
			getUsers();
		}
	}, [])
	const [users, setUsers] = React.useState([]);
	
	const getUsers = async () => {
		const token = localStorage.getItem("token");
		try {
			const res = await fetch('http://localhost/cfd/manage/users', { headers: {"Authorization" : `Bearer ${token}`} });
			if (!res.ok) {
				throw new Error("Fetch was not OK");
			}
			const json = await res.json();
			setUsers(json);
		} catch (error) {
			alert(`Error fetching or parsing data: ${error}`);
	  }		
	};
	
	const Users = ({ items }) => {
	React.useEffect(() => {

	}, [items])
	
	  return (
	  <div><h4>Users:</h4>
	  		{items.map(user => {
			return(
			<div key={user.id} className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px', boxShadow: '1px 1px 10px #808080'}}>
			<b style={{paddingRight: '5px'}}>Username:</b> {user.username}<hr/>
			<Link to={`/profile/manage/users/${user.id}`}>
			<button type="submit" className="btn btn-info" style={{paddingTop: '2px', paddingBottom: '2px', paddingLeft: '15px', paddingRight: '15px', fontSize: '14px'}}>Show</button>
			</Link></div>)})
		}
	  </div>
	)
}

const User = ({itemId}) => {
	React.useEffect(() => {
		document.documentElement.scrollTop = 0;
		const token = localStorage.getItem("token");
		if (token) {
		fetch('http://localhost/cfd/manage/users/'+itemId, { headers: {"Authorization" : `Bearer ${token}`} }).then(function (response) {
			return response.json();
		}).then(function (json) {
			setUser(json)
		}).catch(function (err) {
			console.warn('Something went wrong.', err);
			alert(err)
		})
		}
	}, [itemId])
	
	const [user, setUser] = React.useState(false);
	
	if (!user) {
	  return (
	  <div style={{border: '1px solid lightGray', height: '130px', maxWidth: '800px'}}>
		<img src={'http://eax.arkku.net/loading.gif'} style={{height: '130px', width: '130px', display: 'block', margin: 'auto'}}/>
		</div> 
	  )
	}
	
  return (
  <div>
	<div className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px'}}>
		<b style={{paddingRight: '5px'}}>Id:</b> {user.username}
	</div>
	<div className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px'}}>
		<b style={{paddingRight: '5px'}}>Username:</b> {user.username}
	</div>
	<div className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px'}}>
		<b style={{paddingRight: '5px'}}>Subscription:</b> {user.subscription}
	</div>
  </div>
  )
}
	
return(
  <Switch>
    <Route exact path={['/profile/manage']} render={() => ( <Users items={users} /> )}/>
	<Route path={["/profile/manage/users/:id"]} render={({ match }) => {
		const itemId = match.params.id;
        return <User itemId={itemId} />
    }} />
  </Switch>
)
}
	
return(
	<>
      <h2>Profile</h2>
  <div>	
		<div className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px'}}>
			<b style={{paddingRight: '5px'}}>Id:</b> {user.username}
		</div>
		<div className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px'}}>
			<b style={{paddingRight: '5px'}}>Username:</b> {user.username}
		</div>
		<div className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px'}}>
			<b style={{paddingRight: '5px'}}>Subscription:</b> {user.subscription}
		</div>
	  </div><br/>
	  <nav style={{}}>
      <ul style={{display:'block', paddingLeft: '0px'}}>
	  { allowManage ? <Link to='/profile/manage'><li className='list-inline-item' style={{color: '#007bff', minWidth: '100px', margin: '0px', padding: '10px', border: '1px solid lightGray'}}>Show users list</li></Link> : '' }
      </ul>
	</nav>
      <Switch>
        <Route path={`${path}/manage`}>
          <Manage />
        </Route>
      </Switch>
	  <br/><br/><br/>
    </>
)}

export default ProfileComponent;